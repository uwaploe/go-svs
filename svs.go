// Package svs provides an interface to a Valeport Sound Velocity Sensor.
package svs

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	// Command terminator
	EOT = "\r\n"
	// Response terminator
	EOL = "\r\n"
	// Sensor prompt
	PROMPT = ">"
)

// Valid data rates for streaming mode
type DataRate string

const (
	Rate1Hz  DataRate = "M1"
	Rate2Hz           = "M2"
	Rate4Hz           = "M4"
	Rate8Hz           = "M8"
	Rate16Hz          = "M16"
	Rate32Hz          = "M32"
	Rate60Hz          = "M60"
)

// respScanner parses the command responses from the device
type respScanner struct {
	rdr     io.Reader
	respBuf bytes.Buffer
	prompt  []byte
	tail    int
}

func newScanner(rdr io.Reader, prompt string) *respScanner {
	return &respScanner{
		rdr:    rdr,
		prompt: []byte(prompt),
	}
}

func (s *respScanner) next() (string, error) {
	s.respBuf.Reset()
	s.tail = -len(s.prompt)
	b := make([]byte, 1)
	for {
		_, err := s.rdr.Read(b)
		if err != nil {
			return s.respBuf.String(), err
		}

		s.respBuf.Write(b)
		s.tail++
		if s.tail < 0 {
			continue
		}

		if bytes.Equal(s.respBuf.Bytes()[s.tail:], s.prompt) {
			return strings.TrimSuffix(s.respBuf.String(), string(s.prompt)), nil
		}

	}

	return "", nil
}

// Error produced on an invalid command
type InvalidCommand struct {
	cmd string
}

func (e *InvalidCommand) Error() string {
	return fmt.Sprintf("Invalid command: %q", e.cmd)
}

type Device struct {
	port                  io.ReadWriter
	scanner               *respScanner
	isStreaming           bool
	parseErrors, discards int
}

// New returns a new *Device instance attached to an io.ReadWriter. If
// streaming is true, the device starts in streaming mode.
func New(rw io.ReadWriter, streaming bool) *Device {
	d := Device{
		port:        rw,
		isStreaming: streaming,
	}
	if streaming {
		d.scanner = newScanner(d.port, EOL)
	} else {
		d.scanner = newScanner(d.port, PROMPT)
	}

	return &d
}

func findError(req, resp string) error {
	if idx := strings.Index(resp, "ERROR!"); idx >= 0 {
		return &InvalidCommand{cmd: req}
	}
	return nil
}

// Command sends a command to the device and returns the response along with
// any error that occurred.
func (d *Device) Command(req string) (resp string, err error) {
	if d.isStreaming {
		return "", fmt.Errorf("%q not allowed in streaming mode", req)
	}

	_, err = d.port.Write([]byte(req + EOL))
	if err != nil {
		return "", err
	}

	if req[0] == 'M' {
		d.isStreaming = true
		// No prompt in streaming mode
		d.scanner = newScanner(d.port, EOL)
		return d.scanner.next()
	}

	resp, err = d.scanner.next()
	if err != nil {
		return resp, err
	}

	// Skip over the first line containing the echoed command.
	if idx := strings.Index(resp, EOL); idx >= 0 {
		resp = resp[idx+len(EOL):]
	}

	return resp, findError(req, resp)
}

// Data sample from the device
type Sample struct {
	T  time.Time `json:"t"`
	Sv float32   `json:"sv"`
}

func parseValue(s string) (x float32, err error) {
	_, err = fmt.Sscanf(s, "%f", &x)
	return x, err
}

// Value returns a single data sample
func (d *Device) Value() (s Sample, err error) {
	s.T = time.Now()
	resp, err := d.Command("S")
	if err != nil {
		return s, err
	}

	x, err := parseValue(resp)
	if err != nil {
		return s, errors.Wrap(err, "parse output")
	}

	s.Sv = float32(x)
	return s, err
}

// Stop puts the device in command mode.
func (d *Device) Stop() error {
	if !d.isStreaming {
		return nil
	}
	_, err := d.port.Write([]byte("#"))
	if err != nil {
		return err
	}

	d.isStreaming = false
	d.scanner = newScanner(d.port, PROMPT)
	d.scanner.next()

	return nil
}

// Start puts the device in streaming mode at the specified data rate.
func (d *Device) Start(rate DataRate) error {
	if d.isStreaming {
		err := d.Stop()
		if err != nil {
			return err
		}
	}

	_, err := d.Command(string(rate))
	return err
}

// ReadStream returns a channel which will produce data samples until
// the Context is cancelled or an i/o error occurs.
func (d *Device) ReadStream(ctx context.Context) (<-chan Sample, error) {
	if !d.isStreaming {
		return nil, fmt.Errorf("Not streaming")
	}

	d.parseErrors, d.discards = 0, 0
	ch := make(chan Sample)
	go func() {
		defer close(ch)
		defer d.Stop()
		s := Sample{}
		for {
			resp, err := d.scanner.next()
			if err != nil {
				return
			}
			// If we caught the full data record line, the
			// first character should be a space.
			if resp[0] != ' ' {
				continue
			}
			s.T = time.Now()
			s.Sv, err = parseValue(resp)
			if err != nil {
				d.parseErrors++
				continue
			}

			select {
			case <-ctx.Done():
				return
			case ch <- s:
			default:
				d.discards++
				// Discard sample
			}
		}
	}()

	return ch, nil
}

// Errors returns the error counts from the last streaming session the
// first value is the number of parse errors and the second is the number
// of samples discarded.
func (d *Device) Errors() (int, int) {
	return d.parseErrors, d.discards
}
