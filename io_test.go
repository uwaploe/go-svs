package svs

import (
	"bytes"
	"context"
	"strings"
	"testing"
	"time"
)

type port struct {
	w    *strings.Builder
	r    *strings.Reader
	trig chan struct{}
}

func newPort(trigger chan struct{}) *port {
	p := port{
		w:    &strings.Builder{},
		trig: trigger,
	}
	return &p
}

func (p *port) setReader(s string) {
	p.r = strings.NewReader(s)
}

func (p *port) Read(b []byte) (int, error) {
	<-p.trig
	n, err := p.r.Read(b)
	if b[0] == '\n' {
		time.Sleep(time.Millisecond)
	}
	return n, err
}

func (p *port) Write(b []byte) (int, error) {
	return p.w.Write(b)
}

func TestScanner(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{
			in:  "S\r\n 1234.567\r\n>",
			out: "S\r\n 1234.567\r\n",
		},
		{in: ">", out: ""},
	}

	var rbuf bytes.Buffer
	s := newScanner(&rbuf, PROMPT)
	for _, e := range table {
		rbuf.Write([]byte(e.in))
		resp, _ := s.next()
		if resp != e.out {
			t.Errorf("Mismatch; expected %q, got %q", e.out, resp)
		}
	}
}

func TestStreamScanner(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{
			in:  " 1234.567\r\n",
			out: " 1234.567",
		},
	}

	var rbuf bytes.Buffer
	s := newScanner(&rbuf, EOL)
	for _, e := range table {
		rbuf.Write([]byte(e.in))
		resp, _ := s.next()
		if resp != e.out {
			t.Errorf("Mismatch; expected %q, got %q", e.out, resp)
		}
	}
}

func TestCommand(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	table := []struct {
		in, out, resp string
	}{
		{
			in:   "S",
			out:  "S\r\n 1234.567\r\n>",
			resp: " 1234.567\r\n",
		},
		{
			in:   "#091",
			out:  "#091\r\nOFF\r\n>",
			resp: "OFF\r\n",
		},
	}

	d := New(p, false)
	for _, e := range table {
		p.setReader(e.out)
		resp, err := d.Command(e.in)
		if err != nil {
			t.Error(err)
		}
		if resp != e.resp {
			t.Errorf("Bad response; expected %q, got %q", e.resp, resp)
		}
	}
}

func TestSample(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	close(ch)

	p.setReader("S\r\n 1234.567\r\n>")
	expected := float32(1234.567)

	d := New(p, false)
	x, err := d.Value()
	if err != nil {
		t.Fatal(err)
	}
	if x.Sv != expected {
		t.Errorf("Bad sample; expected %v, got %v", expected, x.Sv)
	}
}

func TestStream(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	values := []string{" 1234.567", " 1234.567", " 1234.567\r\n"}
	expected := float32(1234.567)

	p.setReader(strings.Join(values, "\r\n"))

	n := int(0)
	d := New(p, true)
	stream, err := d.ReadStream(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		time.Sleep(time.Second)
		close(ch)
	}()

	for s := range stream {
		if s.Sv != expected {
			t.Errorf("Bad sample; expected %v, got %v", expected, s.Sv)
		}
		n++
	}

	if n != len(values) {
		t.Errorf("Bad value count; expected %d, got %d", len(values), n)
	}
}

func TestShortStream(t *testing.T) {
	ch := make(chan struct{})
	p := newPort(ch)
	values := []string{"34.567", " 1234.567", " 1234.567\r\n"}
	expected := float32(1234.567)

	p.setReader(strings.Join(values, "\r\n"))

	n := int(0)
	d := New(p, true)
	stream, err := d.ReadStream(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	go func() {
		time.Sleep(time.Second)
		close(ch)
	}()

	for s := range stream {
		if s.Sv != expected {
			t.Errorf("Bad sample; expected %v, got %v", expected, s.Sv)
		}
		n++
	}

	if n != len(values)-1 {
		t.Errorf("Bad value count; expected %d, got %d", len(values)-1, n)
	}
}
